package com.arun.aashu.login_page;

import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.MenuItem;

public class Second_navDrawer extends AppCompatActivity {

    private DrawerLayout mdrawerLayout1;
    private ActionBarDrawerToggle mToggle;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_second_nav_drawer);

        mdrawerLayout1=findViewById(R.id.seondNavdrawer);
        mToggle=new ActionBarDrawerToggle(this,mdrawerLayout1,R.string.open,R.string.close);
        mdrawerLayout1.addDrawerListener(mToggle);
        mToggle.syncState();
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

    }
    public boolean onOptionsItemSelected(MenuItem item) {
        if (mToggle.onOptionsItemSelected(item)){
            return  true;
        }

        return super.onOptionsItemSelected(item);
    }


    }

